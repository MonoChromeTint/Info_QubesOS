#!/bin/bash

# Reset:
#	find . -type f -not -path "*/.git/*" -exec rm '{}' \;
#	find . -type d -not -path "*.git*" -exec rmdir '{}' \;

#GIT_VM="sys-Sync"
GIT_VM="Coding-pve"

function CODING_VM {
	echo "Coding VM"
	for i in $(find . -maxdepth 1 -type d -print); do
		if [[ "$i" != '.' ]]; then
			RFolders="$RFolders $i"
		fi
	done

	qvm-copy $RFolders
}

function GIT_VM {
	echo "Git VM"

#	find . -type f -iname "*.swp" -not -path "*/.git/*" -exec rm '{}' \;

#	for i in $(find . -maxdepth 1 -type d -print); do
#		DirectoryName=$(echo "$i" | cut -d'/' -f2)
#		if [[ "$i" != '.' ]] && [[ ! "$DirectoryName" =~ [A-Z] ]]; then
#			cd "$i"
#			mkdir deleting
#			mv * deleting
#			rm -dR deleting
#			cd ..
#		fi
#	done
#	for i in $(find . -maxdepth 1 -type d -print); do
#		DirectoryName=$(echo "$i" | cut -d'/' -f2)
#		if [[ "$i" != '.' ]] && [[ "$DirectoryName" =~ [A-Z] ]]; then
#			RepoName=$(echo "$i" | tr '[:upper:]' '[:lower:]')
#			if [[ -d "$RepoName" ]]; then
#				mv "$i"/* "$RepoName"
#				rm -d "$i"
#			else
#				mv "$i" "$RepoName"
#			fi
#		fi
#	done



	for i in $(find . -maxdepth 1 -type d -print); do
		DirectoryName=$(echo "$i" | cut -d'/' -f2)
		if [[ "$i" != '.' ]] && [[ "$DirectoryName" =~ [a-z] ]]; then
			echo "$DirectoryName"
			cd "$DirectoryName"
			git add . && git commit -m "$(date +%Y_%m_%d_%s)" && git push -u origin master
			cd ..
			echo
		fi
	done


}



if [[ "$HOSTNAME" != "$GIT_VM" ]]; then
	CODING_VM
else
	GIT_VM
fi
