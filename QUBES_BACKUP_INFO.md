
# Manually unpack Qubes VM(s)

## Untar initial backup
tar -i -xvf qubes-backup-2023-$NUM

## Set Variables
cat backup-header

backup_id=$(cat backup-header | grep backup_id | cut -d'=' -f2)

read -p "backup_pass: " backup_pass


exit

## Password Format
### Format: $backup_id ! $vm ! $backup_pass
###                   ^ no spaces

echo "$backup_id"'!'"VM_Name.xml.000"'!'"$backup_pass"

## IDing individual VMs

scrypt dec -P qubes.xml.000.enc qubes.xml.000

## Replace BOTH "vm1" with targetted vm number
find vm1 -name 'private.img.*.enc' | sort -V | while read f_enc; do \
    f_dec=${f_enc%.enc}; \
    echo "$backup_id"'!'"$f_dec"'!'"$backup_pass" | scrypt dec -P $f_enc || break; \
    done | gzip -d | tar -xv vm1/private.img


## Mount private.img
sudo mkdir /media/img
sudo mount -o loop vm1/private.img /media/img
cat /mnt/img/home/user/your_data.txt
